module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = local.key_name
  public_key = file(var.public_key_path)
}
