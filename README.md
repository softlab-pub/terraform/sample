# terraformサンプル

terraformのシンプルなサンプルです。

## 環境

* aws-cli/1.14.44
* Terraform v0.12.2
* direnv 2.15.0
* python3 venv

## 設定

terraform用のアクセスキーを作成して、下記のコマンドで``terraform``プロファイルを設定します。

```
aws --profile terraform configure
```

設定は、下記のコマンドで確認できます。

```
export AWS_DEFAULT_PROFILE=terraform
aws configure list
aws sts get-caller-identity
```

## サンプルコードの取得

```
git clone https://gitlab.com/softlab-pub/terraform/sample.git
cd sample
```

## サンプル一覧

| ディレクトリ    | 内容                             |
|:----------------|----------------------------------|
| vpc/default_vpc | デフォルトVPCのVPC IDを参照する  |
| eks/default_vpc | デフォルトVPCにEKSクラスタを作成 |

## 使い方

サンプルは、下記の操作で初期化、確認、作成、削除することができます。

### 初期化

```
make setup
TARGET=<ディレクトリ>
cd $TARGET
terraform init
```

### 確認

変更されるリソースの情報が確認できます。

```
terraform plan
```

### 作成

```
terraform apply
```

### 削除

```
terraform destroy
```

以上
