data "aws_vpc" "target_vpc" {
  default = true
}

data "aws_subnet_ids" "target_subnet_ids" {
  vpc_id = data.aws_vpc.target_vpc.id
}
