output "vpc_id" {
  value = local.vpc_id
}
output "cluster_name" {
  value = local.cluster_name
}
