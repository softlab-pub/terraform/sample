variable "cluster_name" {
  default = "sample"
}
variable "instance_type" {
  default = "t3.medium"
}
variable "node_count" {
  default = 2
}
