locals {
  vpc_id	= data.aws_vpc.target_vpc.id
  subnet_ids	= data.aws_subnet_ids.target_subnet_ids.ids
  cluster_name  = format("%s-%s", var.cluster_name, terraform.workspace)
}
