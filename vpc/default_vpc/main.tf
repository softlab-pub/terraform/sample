provider "aws" {
  profile = "terraform"
  region  = "ap-northeast-1"
}

data "aws_vpc" "target_vpc" {
  default = true
}

locals {
  vpc_id = data.aws_vpc.target_vpc
}

output "vpc_id" {
  value = local.vpc_id.id
}

