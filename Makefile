TERRAFORM_VER=0.12.25

.SILENT:

all:

setup: terraform kubectl aws-iam-authenticator

### kubectl https://kubernetes.io/ja/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux
.PHONY: kubectl
kubectl: /usr/local/bin/kubectl
	$@ version --client

/usr/local/bin/kubectl:
	$(eval VERSION := $(shell curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt))
	@echo Latest version $(VERSION)
	curl -LO https://storage.googleapis.com/kubernetes-release/release/$(VERSION)/bin/linux/amd64/kubectl
	chmod +x ./kubectl
	sudo mv ./kubectl $@

### aws-iam-authenticator https://docs.aws.amazon.com/ja_jp/eks/latest/userguide/install-aws-iam-authenticator.html
.PHONY: aws-iam-authenticator
aws-iam-authenticator: /usr/local/bin/aws-iam-authenticator
	$@ version

/usr/local/bin/aws-iam-authenticator:
	curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator
	curl -o aws-iam-authenticator.sha256 https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator.sha256
	openssl sha1 -sha256 aws-iam-authenticator
	chmod +x ./aws-iam-authenticator
	sudo cp ./aws-iam-authenticator $@
	rm aws-iam-authenticator aws-iam-authenticator.sha256

### Install/Upgrade terraform
.PHONY: terraform
terraform:  /usr/local/bin/terraform
	terraform --version

/usr/local/bin/terraform:
	curl https://releases.hashicorp.com/terraform/$(TERRAFORM_VER)/terraform_$(TERRAFORM_VER)_linux_amd64.zip -o terraform.zip
	unzip -o terraform.zip
	rm -f terraform.zip
	sudo mv terraform $@
	-terraform -install-autocomplete
